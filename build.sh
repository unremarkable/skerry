#!/usr/bin/env bash

echo 'Building...'

# https://stackoverflow.com/a/246128/738081
cd $(dirname "${BASH_SOURCE[0]}")

mkdir -p dist
> dist/skerry.js

SRC_FILES="
  src/main.js
  src/aux.js
"

for file in $SRC_FILES
do
  cat $file >> dist/skerry.js
done
